/**
 * crossoverAuction 
 * @author Charan Raj T C <charanraj.tc@gmail.com>
 */

/*globals $:false */
/*globals document:false */
/*globals window:false */
/*globals angular:false */

// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function() {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Place any jQuery/helper plugins in here.

//Doument ready function 
$(document).ready(function() {
    console.log("Hello Document Loaded ");
});


//adding underscore to angular http://underscorejs.org/
var underscore = angular.module('underscore', []);
underscore.factory('_', [function() {
    //return the underscore object 
    return window._; //Underscore must already be loaded on the page
}]);


//initialize angular application here 

var indecabtest = angular.module('indecabtest', ['underscore']);


// maincontroller for the application 

indecabtest.controller('mainController', ['$rootScope', '$scope',
    function($rootScope, $scope) {
        console.log('Main contoller loaded ');


    }
]);
